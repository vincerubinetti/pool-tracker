
class App extends React.Component
{
	constructor()
	{
		super();
		this.state = { page: 'players', players: [], games: [] };
		this.onMenuClick = this.onMenuClick.bind(this);
		this.addPlayer = this.addPlayer.bind(this);
		this.tallyGames = this.tallyGames.bind(this);
		this.addGame = this.addGame.bind(this);
	}
	onMenuClick(event)
	{
		this.setState( { page: event.target.dataset.name } );
	}
	addPlayer(name, catchphrase, cueLength)
	{
		this.setState(
			function(state, props)
			{
				var newPlayers = state.players.concat(new Player(name, catchphrase, cueLength))
				return { players: newPlayers };
			}
		);
	}
	tallyGames(gameList)
	{
		var newPlayers = this.state.players;
		for(var player of newPlayers)
		{
			var games = 0;
			var wins = 0;
			for(var game of Array.from(gameList))
			{
				if (player.uid === game.idA || player.uid === game.idB)
					games++;
				if ((player.uid === game.idA && game.isWinnerA) || (player.uid === game.idB && !game.isWinnerA))
					wins++;
			}
			player.games = games;
			player.wins = wins;
		}
		this.setState( { players: newPlayers } );
	}
	addGame(idA, idB, isWinnerA)
	{
		this.setState(
			function(state, props)
			{
				var playerA, playerB;
				for(var player of state.players)
				{
					if (player.uid === idA)
						playerA = player;
					if (player.uid === idB)
						playerB = player;
				}
				var newGames = state.games.concat(new Game(idA, idB, isWinnerA, playerA.name, playerB.name, playerA.catchphrase, playerB.catchphrase))
				this.tallyGames(newGames);
				return { games: newGames };
			}
		);
	}
	render()
	{
		return (
			<div id='body_container'>
				<Menu activePage={this.state.page} onMenuClick={this.onMenuClick}/>
				<Page activePage={this.state.page} players={this.state.players} games={this.state.games} addPlayer={this.addPlayer} addGame={this.addGame}/>
			</div>
		);
	}
}
class Menu extends React.Component
{
	render()
	{
		return (
			<div id='menu_container'>
				<div id='menu_logo' className='menu_header'>
					<img id='logo' src='assets/logo.svg'/>
					<span className='fancy'>Billiards</span>&nbsp;
					<span className='bold'>Tracker</span>
				</div>
				<button className='menu_option bold' onClick={this.props.onMenuClick} data-active={this.props.activePage === 'players' ? true : false} data-name='players'>Players</button>
				<button className='menu_option bold' onClick={this.props.onMenuClick} data-active={this.props.activePage === 'games' ? true : false} data-name='games'>Games</button>
				<button className='menu_option bold' onClick={this.props.onMenuClick} data-active={this.props.activePage === 'leaderboard' ? true : false} data-name='leaderboard'>Leadboard</button>
			</div>
		);
	}
}
class Page extends React.Component
{
	getActivePage()
	{
		switch(this.props.activePage)
		{
			case 'players': return <PlayersPage players={this.props.players} addPlayer={this.props.addPlayer}/>;
			case 'games': return <GamesPage players={this.props.players} games={this.props.games} addGame={this.props.addGame}/>;
			case 'leaderboard': return <LeadboardPage players={this.props.players}/>;
			default: return null;
		}
	}
	render()
	{
		return (
			<div id='page_container'>
				{this.getActivePage()}
			</div>
		);
		
	}
}
class PlayersPage extends React.Component
{
	render()
	{
		var playerCards = this.props.players.map((player) => <PlayerCard key={player.uid} name={player.name} catchphrase={player.catchphrase} cueLength={player.cueLength} games={player.games} wins={player.wins}/> );
		return (
			<div className='page'>
				<p className='bold'>
					Players
				</p>
				{playerCards}
				<NewPlayerCard addPlayer={this.props.addPlayer}/>
			</div>
		);
	}
}
class GamesPage extends React.Component
{
	render()
	{
		var game = this.props.games.map((game, index) => <GameCard key={index} isWinnerA={game.isWinnerA} nameA={game.nameA} nameB={game.nameB} catchphraseA={game.catchphraseA} catchphraseB={game.catchphraseB}/> );
		return (
			<div className='page'>
				<p className='bold'>
					Games
				</p>
				{game}
				<NewGameCard players={this.props.players} addGame={this.props.addGame}/>
			</div>
		);
	}
}
class LeadboardPage extends React.Component
{
	render()
	{
		var playerBars = this.props.players.sort((a, b) => b.wins-a.wins || b.games-a.games);
		playerBars = playerBars.map((player, index) => <PlayerBar key={player.uid} count={index+1} name={player.name} catchphrase={player.catchphrase} games={player.games} wins={player.wins}/> );
		return (
			<div className='page'>
				<p className='bold'>
					Leaderboard
				</p>
				{playerBars}
			</div>
		);
	}
}
class PlayerCard extends React.Component
{
	render()
	{
		return (
			<div className='card'>
				<img src='assets/avatar.svg' className='avatar'/>
				<p className='bold'>{this.props.name}</p>
				<p>"{this.props.catchphrase}"</p>
				<p>{this.props.cueLength} inch cue</p>
				<p>{this.props.games} games</p>
				<p>{this.props.wins} wins</p>
			</div>
		);
	}
}
class NewPlayerCard extends React.Component
{
	constructor(props)
	{
		super(props);
		this.submit = this.submit.bind(this);
	}
	submit()
	{
		this.props.addPlayer(document.getElementById('new_name').value, document.getElementById('new_catchphrase').value, document.getElementById('new_cueLength').value);
		document.getElementById('new_name').value = '';
		document.getElementById('new_catchphrase').value = '';
		document.getElementById('new_cueLength').value = '';
	}
	render()
	{
		return (
			<div className='card'>
				<p>
					Name:<br/>
					<input type='text' id='new_name'></input>
				</p>
				<p>
					Catchphrase:<br/>
					<input type='text' id='new_catchphrase'></input>
				</p>
				<p>
					Cue Length (inches):<br/>
					<input type='number' id='new_cueLength'></input>
				</p>
				<button className='bold' onClick={this.submit}>Add New +</button>
			</div>
		);
	}
}
class GameCard extends React.Component
{
	render()
	{
		return (
			<div className='card'>
				<div className='results_player' data-winner={this.props.isWinnerA}>
					<img src='assets/avatar.svg' className='avatar_mini'/>
					<p>{this.props.nameA}</p>
					<p>"{this.props.catchphraseA}"</p>
				</div>
				<p className='bold center'>
					vs
				</p>
				<div className='results_player' data-winner={!this.props.isWinnerA}>
					<img src='assets/avatar.svg' className='avatar_mini'/>
					<p>{this.props.nameB}</p>
					<p>"{this.props.catchphraseB}"</p>
				</div>
			</div>
		);
	}
}
class NewGameCard extends React.Component
{
	constructor(props)
	{
		super(props);
		this.submit = this.submit.bind(this);
	}
	submit()
	{
		var idA = document.getElementById('new_player_A').options[document.getElementById('new_player_A').selectedIndex].dataset.uid;
		var idB = document.getElementById('new_player_B').options[document.getElementById('new_player_B').selectedIndex].dataset.uid;
		var isWinnerA = document.getElementById('new_winner_A').checked;
		var isWinnerB = document.getElementById('new_winner_B').checked;

		if (!idA || !idB || idA === idB || (isWinnerA && isWinnerB) || (!isWinnerA && !isWinnerB))
			return;


		this.props.addGame(idA, idB, isWinnerA);

		document.getElementById('new_player_A').selectedIndex = 0;
		document.getElementById('new_player_B').selectedIndex = 0;
		document.getElementById('new_winner_A').checked = false;
		document.getElementById('new_winner_B').checked = false;
	}
	render()
	{
		var options = this.props.players.map((player) => <option key={player.uid} data-uid={player.uid}>{player.name} ({player.catchphrase})</option> );
		return (
			<div className='card'>
				<p>
					Player A:<br/>
					<select type='text' id='new_player_A'>
					<option></option>
					{options}
					</select>
				</p>
				<p className='bold center'>
					vs
				</p>
				<p>
					Player B:<br/>
					<select type='text' id='new_player_B'>
					<option></option>
					{options}
					</select>
				</p>
				<p>
					&nbsp;
				</p>
				<p>
					Winner:<br/>
					<input type='radio' id='new_winner_A' name='winner_radio'/>
					<label htmlFor='new_winner_A'>Player A</label><br/>
					<input type='radio' id='new_winner_B' name='winner_radio'/>
					<label htmlFor='new_winner_B'>Player B</label>
				</p>
				<button className='bold' onClick={this.submit}>Add New +</button>
			</div>
		);
	}
}
class PlayerBar extends React.Component
{
	render()
	{
		return (
			<div className='bar'>
				<div className='bold'>
					{this.props.count}
				</div>
				<div>
					<img src='assets/avatar.svg' className='avatar_small'/>
				</div>
				<div>
					{this.props.name}
				</div>
				<div>
					{this.props.catchphrase}
				</div>
				<div>
					{this.props.games}<br/>
					games
				</div>
				<div>
					{this.props.wins}<br/>
					wins
				</div>
			</div>
		);
	}
}
class Player
{
	constructor(name, catchphrase, cueLength, uid)
	{
		// defaults
		var names = ['John Doe', 'Jane Doe', 'John Smith', 'Jane Smith', 'Anne Chovie', 'Max Power', 'Burt Toast', 'Jay Walker', 'Gerry Atrick'];
		var catchphrases = ['Things are getting too spicy for the pepper!', 'Is that your final answer?', 'Resistance is futile.', 'Did I do that?', 'Find your own beach house, Crabby.']
		var cueLengths = [42, 52, 56, 58, 60, 62];

		this.name = name || names.rand();
		this.catchphrase = catchphrase || catchphrases.rand();
		this.cueLength = cueLength || cueLengths.rand();
		this.games = 0;
		this.wins = 0;
		this.uid = uid || String(Date.now()); // quick pseudo-unique id
	}
}
class Game
{
	constructor(idA, idB, isWinnerA, nameA, nameB, catchphraseA, catchphraseB)
	{
		this.idA = idA;
		this.idB = idB;
		this.isWinnerA = isWinnerA;
		this.nameA = nameA;
		this.nameB = nameB;
		this.catchphraseA = catchphraseA;
		this.catchphraseB = catchphraseB;
	}
}
window.onload = function()
{
	ReactDOM.render(<App/>, document.getElementById('root'));
}
Array.prototype.rand = function()
{
	return this[Math.floor(Math.random()*this.length)];
}
