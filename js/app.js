'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var App = function (_React$Component) {
	_inherits(App, _React$Component);

	function App() {
		_classCallCheck(this, App);

		var _this = _possibleConstructorReturn(this, (App.__proto__ || Object.getPrototypeOf(App)).call(this));

		_this.state = { page: 'players', players: [], games: [] };
		_this.onMenuClick = _this.onMenuClick.bind(_this);
		_this.addPlayer = _this.addPlayer.bind(_this);
		_this.tallyGames = _this.tallyGames.bind(_this);
		_this.addGame = _this.addGame.bind(_this);
		return _this;
	}

	_createClass(App, [{
		key: 'onMenuClick',
		value: function onMenuClick(event) {
			this.setState({ page: event.target.dataset.name });
		}
	}, {
		key: 'addPlayer',
		value: function addPlayer(name, catchphrase, cueLength) {
			this.setState(function (state, props) {
				var newPlayers = state.players.concat(new Player(name, catchphrase, cueLength));
				return { players: newPlayers };
			});
		}
	}, {
		key: 'tallyGames',
		value: function tallyGames(gameList) {
			var newPlayers = this.state.players;
			var _iteratorNormalCompletion = true;
			var _didIteratorError = false;
			var _iteratorError = undefined;

			try {
				for (var _iterator = newPlayers[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
					var player = _step.value;

					var games = 0;
					var wins = 0;
					var _iteratorNormalCompletion2 = true;
					var _didIteratorError2 = false;
					var _iteratorError2 = undefined;

					try {
						for (var _iterator2 = Array.from(gameList)[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
							var game = _step2.value;

							if (player.uid === game.idA || player.uid === game.idB) games++;
							if (player.uid === game.idA && game.isWinnerA || player.uid === game.idB && !game.isWinnerA) wins++;
						}
					} catch (err) {
						_didIteratorError2 = true;
						_iteratorError2 = err;
					} finally {
						try {
							if (!_iteratorNormalCompletion2 && _iterator2.return) {
								_iterator2.return();
							}
						} finally {
							if (_didIteratorError2) {
								throw _iteratorError2;
							}
						}
					}

					player.games = games;
					player.wins = wins;
				}
			} catch (err) {
				_didIteratorError = true;
				_iteratorError = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion && _iterator.return) {
						_iterator.return();
					}
				} finally {
					if (_didIteratorError) {
						throw _iteratorError;
					}
				}
			}

			this.setState({ players: newPlayers });
		}
	}, {
		key: 'addGame',
		value: function addGame(idA, idB, isWinnerA) {
			this.setState(function (state, props) {
				var playerA, playerB;
				var _iteratorNormalCompletion3 = true;
				var _didIteratorError3 = false;
				var _iteratorError3 = undefined;

				try {
					for (var _iterator3 = state.players[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var player = _step3.value;

						if (player.uid === idA) playerA = player;
						if (player.uid === idB) playerB = player;
					}
				} catch (err) {
					_didIteratorError3 = true;
					_iteratorError3 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError3) {
							throw _iteratorError3;
						}
					}
				}

				var newGames = state.games.concat(new Game(idA, idB, isWinnerA, playerA.name, playerB.name, playerA.catchphrase, playerB.catchphrase));
				this.tallyGames(newGames);
				return { games: newGames };
			});
		}
	}, {
		key: 'render',
		value: function render() {
			return React.createElement(
				'div',
				{ id: 'body_container' },
				React.createElement(Menu, { activePage: this.state.page, onMenuClick: this.onMenuClick }),
				React.createElement(Page, { activePage: this.state.page, players: this.state.players, games: this.state.games, addPlayer: this.addPlayer, addGame: this.addGame })
			);
		}
	}]);

	return App;
}(React.Component);

var Menu = function (_React$Component2) {
	_inherits(Menu, _React$Component2);

	function Menu() {
		_classCallCheck(this, Menu);

		return _possibleConstructorReturn(this, (Menu.__proto__ || Object.getPrototypeOf(Menu)).apply(this, arguments));
	}

	_createClass(Menu, [{
		key: 'render',
		value: function render() {
			return React.createElement(
				'div',
				{ id: 'menu_container' },
				React.createElement(
					'div',
					{ id: 'menu_logo', className: 'menu_header' },
					React.createElement('img', { id: 'logo', src: 'assets/logo.svg' }),
					React.createElement(
						'span',
						{ className: 'fancy' },
						'Billiards'
					),
					'\xA0',
					React.createElement(
						'span',
						{ className: 'bold' },
						'Tracker'
					)
				),
				React.createElement(
					'button',
					{ className: 'menu_option bold', onClick: this.props.onMenuClick, 'data-active': this.props.activePage === 'players' ? true : false, 'data-name': 'players' },
					'Players'
				),
				React.createElement(
					'button',
					{ className: 'menu_option bold', onClick: this.props.onMenuClick, 'data-active': this.props.activePage === 'games' ? true : false, 'data-name': 'games' },
					'Games'
				),
				React.createElement(
					'button',
					{ className: 'menu_option bold', onClick: this.props.onMenuClick, 'data-active': this.props.activePage === 'leaderboard' ? true : false, 'data-name': 'leaderboard' },
					'Leadboard'
				)
			);
		}
	}]);

	return Menu;
}(React.Component);

var Page = function (_React$Component3) {
	_inherits(Page, _React$Component3);

	function Page() {
		_classCallCheck(this, Page);

		return _possibleConstructorReturn(this, (Page.__proto__ || Object.getPrototypeOf(Page)).apply(this, arguments));
	}

	_createClass(Page, [{
		key: 'getActivePage',
		value: function getActivePage() {
			switch (this.props.activePage) {
				case 'players':
					return React.createElement(PlayersPage, { players: this.props.players, addPlayer: this.props.addPlayer });
				case 'games':
					return React.createElement(GamesPage, { players: this.props.players, games: this.props.games, addGame: this.props.addGame });
				case 'leaderboard':
					return React.createElement(LeadboardPage, { players: this.props.players });
				default:
					return null;
			}
		}
	}, {
		key: 'render',
		value: function render() {
			return React.createElement(
				'div',
				{ id: 'page_container' },
				this.getActivePage()
			);
		}
	}]);

	return Page;
}(React.Component);

var PlayersPage = function (_React$Component4) {
	_inherits(PlayersPage, _React$Component4);

	function PlayersPage() {
		_classCallCheck(this, PlayersPage);

		return _possibleConstructorReturn(this, (PlayersPage.__proto__ || Object.getPrototypeOf(PlayersPage)).apply(this, arguments));
	}

	_createClass(PlayersPage, [{
		key: 'render',
		value: function render() {
			var playerCards = this.props.players.map(function (player) {
				return React.createElement(PlayerCard, { key: player.uid, name: player.name, catchphrase: player.catchphrase, cueLength: player.cueLength, games: player.games, wins: player.wins });
			});
			return React.createElement(
				'div',
				{ className: 'page' },
				React.createElement(
					'p',
					{ className: 'bold' },
					'Players'
				),
				playerCards,
				React.createElement(NewPlayerCard, { addPlayer: this.props.addPlayer })
			);
		}
	}]);

	return PlayersPage;
}(React.Component);

var GamesPage = function (_React$Component5) {
	_inherits(GamesPage, _React$Component5);

	function GamesPage() {
		_classCallCheck(this, GamesPage);

		return _possibleConstructorReturn(this, (GamesPage.__proto__ || Object.getPrototypeOf(GamesPage)).apply(this, arguments));
	}

	_createClass(GamesPage, [{
		key: 'render',
		value: function render() {
			var game = this.props.games.map(function (game, index) {
				return React.createElement(GameCard, { key: index, isWinnerA: game.isWinnerA, nameA: game.nameA, nameB: game.nameB, catchphraseA: game.catchphraseA, catchphraseB: game.catchphraseB });
			});
			return React.createElement(
				'div',
				{ className: 'page' },
				React.createElement(
					'p',
					{ className: 'bold' },
					'Games'
				),
				game,
				React.createElement(NewGameCard, { players: this.props.players, addGame: this.props.addGame })
			);
		}
	}]);

	return GamesPage;
}(React.Component);

var LeadboardPage = function (_React$Component6) {
	_inherits(LeadboardPage, _React$Component6);

	function LeadboardPage() {
		_classCallCheck(this, LeadboardPage);

		return _possibleConstructorReturn(this, (LeadboardPage.__proto__ || Object.getPrototypeOf(LeadboardPage)).apply(this, arguments));
	}

	_createClass(LeadboardPage, [{
		key: 'render',
		value: function render() {
			var playerBars = this.props.players.sort(function (a, b) {
				return b.wins - a.wins || b.games - a.games;
			});
			playerBars = playerBars.map(function (player, index) {
				return React.createElement(PlayerBar, { key: player.uid, count: index + 1, name: player.name, catchphrase: player.catchphrase, games: player.games, wins: player.wins });
			});
			return React.createElement(
				'div',
				{ className: 'page' },
				React.createElement(
					'p',
					{ className: 'bold' },
					'Leaderboard'
				),
				playerBars
			);
		}
	}]);

	return LeadboardPage;
}(React.Component);

var PlayerCard = function (_React$Component7) {
	_inherits(PlayerCard, _React$Component7);

	function PlayerCard() {
		_classCallCheck(this, PlayerCard);

		return _possibleConstructorReturn(this, (PlayerCard.__proto__ || Object.getPrototypeOf(PlayerCard)).apply(this, arguments));
	}

	_createClass(PlayerCard, [{
		key: 'render',
		value: function render() {
			return React.createElement(
				'div',
				{ className: 'card' },
				React.createElement('img', { src: 'assets/avatar.svg', className: 'avatar' }),
				React.createElement(
					'p',
					{ className: 'bold' },
					this.props.name
				),
				React.createElement(
					'p',
					null,
					'"',
					this.props.catchphrase,
					'"'
				),
				React.createElement(
					'p',
					null,
					this.props.cueLength,
					' inch cue'
				),
				React.createElement(
					'p',
					null,
					this.props.games,
					' games'
				),
				React.createElement(
					'p',
					null,
					this.props.wins,
					' wins'
				)
			);
		}
	}]);

	return PlayerCard;
}(React.Component);

var NewPlayerCard = function (_React$Component8) {
	_inherits(NewPlayerCard, _React$Component8);

	function NewPlayerCard(props) {
		_classCallCheck(this, NewPlayerCard);

		var _this8 = _possibleConstructorReturn(this, (NewPlayerCard.__proto__ || Object.getPrototypeOf(NewPlayerCard)).call(this, props));

		_this8.submit = _this8.submit.bind(_this8);
		return _this8;
	}

	_createClass(NewPlayerCard, [{
		key: 'submit',
		value: function submit() {
			this.props.addPlayer(document.getElementById('new_name').value, document.getElementById('new_catchphrase').value, document.getElementById('new_cueLength').value);
			document.getElementById('new_name').value = '';
			document.getElementById('new_catchphrase').value = '';
			document.getElementById('new_cueLength').value = '';
		}
	}, {
		key: 'render',
		value: function render() {
			return React.createElement(
				'div',
				{ className: 'card' },
				React.createElement(
					'p',
					null,
					'Name:',
					React.createElement('br', null),
					React.createElement('input', { type: 'text', id: 'new_name' })
				),
				React.createElement(
					'p',
					null,
					'Catchphrase:',
					React.createElement('br', null),
					React.createElement('input', { type: 'text', id: 'new_catchphrase' })
				),
				React.createElement(
					'p',
					null,
					'Cue Length (inches):',
					React.createElement('br', null),
					React.createElement('input', { type: 'number', id: 'new_cueLength' })
				),
				React.createElement(
					'button',
					{ className: 'bold', onClick: this.submit },
					'Add New +'
				)
			);
		}
	}]);

	return NewPlayerCard;
}(React.Component);

var GameCard = function (_React$Component9) {
	_inherits(GameCard, _React$Component9);

	function GameCard() {
		_classCallCheck(this, GameCard);

		return _possibleConstructorReturn(this, (GameCard.__proto__ || Object.getPrototypeOf(GameCard)).apply(this, arguments));
	}

	_createClass(GameCard, [{
		key: 'render',
		value: function render() {
			return React.createElement(
				'div',
				{ className: 'card' },
				React.createElement(
					'div',
					{ className: 'results_player', 'data-winner': this.props.isWinnerA },
					React.createElement('img', { src: 'assets/avatar.svg', className: 'avatar_mini' }),
					React.createElement(
						'p',
						null,
						this.props.nameA
					),
					React.createElement(
						'p',
						null,
						'"',
						this.props.catchphraseA,
						'"'
					)
				),
				React.createElement(
					'p',
					{ className: 'bold center' },
					'vs'
				),
				React.createElement(
					'div',
					{ className: 'results_player', 'data-winner': !this.props.isWinnerA },
					React.createElement('img', { src: 'assets/avatar.svg', className: 'avatar_mini' }),
					React.createElement(
						'p',
						null,
						this.props.nameB
					),
					React.createElement(
						'p',
						null,
						'"',
						this.props.catchphraseB,
						'"'
					)
				)
			);
		}
	}]);

	return GameCard;
}(React.Component);

var NewGameCard = function (_React$Component10) {
	_inherits(NewGameCard, _React$Component10);

	function NewGameCard(props) {
		_classCallCheck(this, NewGameCard);

		var _this10 = _possibleConstructorReturn(this, (NewGameCard.__proto__ || Object.getPrototypeOf(NewGameCard)).call(this, props));

		_this10.submit = _this10.submit.bind(_this10);
		return _this10;
	}

	_createClass(NewGameCard, [{
		key: 'submit',
		value: function submit() {
			var idA = document.getElementById('new_player_A').options[document.getElementById('new_player_A').selectedIndex].dataset.uid;
			var idB = document.getElementById('new_player_B').options[document.getElementById('new_player_B').selectedIndex].dataset.uid;
			var isWinnerA = document.getElementById('new_winner_A').checked;
			var isWinnerB = document.getElementById('new_winner_B').checked;

			if (!idA || !idB || idA === idB || isWinnerA && isWinnerB || !isWinnerA && !isWinnerB) return;

			this.props.addGame(idA, idB, isWinnerA);

			document.getElementById('new_player_A').selectedIndex = 0;
			document.getElementById('new_player_B').selectedIndex = 0;
			document.getElementById('new_winner_A').checked = false;
			document.getElementById('new_winner_B').checked = false;
		}
	}, {
		key: 'render',
		value: function render() {
			var options = this.props.players.map(function (player) {
				return React.createElement(
					'option',
					{ key: player.uid, 'data-uid': player.uid },
					player.name,
					' (',
					player.catchphrase,
					')'
				);
			});
			return React.createElement(
				'div',
				{ className: 'card' },
				React.createElement(
					'p',
					null,
					'Player A:',
					React.createElement('br', null),
					React.createElement(
						'select',
						{ type: 'text', id: 'new_player_A' },
						React.createElement('option', null),
						options
					)
				),
				React.createElement(
					'p',
					{ className: 'bold center' },
					'vs'
				),
				React.createElement(
					'p',
					null,
					'Player B:',
					React.createElement('br', null),
					React.createElement(
						'select',
						{ type: 'text', id: 'new_player_B' },
						React.createElement('option', null),
						options
					)
				),
				React.createElement(
					'p',
					null,
					'\xA0'
				),
				React.createElement(
					'p',
					null,
					'Winner:',
					React.createElement('br', null),
					React.createElement('input', { type: 'radio', id: 'new_winner_A', name: 'winner_radio' }),
					React.createElement(
						'label',
						{ htmlFor: 'new_winner_A' },
						'Player A'
					),
					React.createElement('br', null),
					React.createElement('input', { type: 'radio', id: 'new_winner_B', name: 'winner_radio' }),
					React.createElement(
						'label',
						{ htmlFor: 'new_winner_B' },
						'Player B'
					)
				),
				React.createElement(
					'button',
					{ className: 'bold', onClick: this.submit },
					'Add New +'
				)
			);
		}
	}]);

	return NewGameCard;
}(React.Component);

var PlayerBar = function (_React$Component11) {
	_inherits(PlayerBar, _React$Component11);

	function PlayerBar() {
		_classCallCheck(this, PlayerBar);

		return _possibleConstructorReturn(this, (PlayerBar.__proto__ || Object.getPrototypeOf(PlayerBar)).apply(this, arguments));
	}

	_createClass(PlayerBar, [{
		key: 'render',
		value: function render() {
			return React.createElement(
				'div',
				{ className: 'bar' },
				React.createElement(
					'div',
					{ className: 'bold' },
					this.props.count
				),
				React.createElement(
					'div',
					null,
					React.createElement('img', { src: 'assets/avatar.svg', className: 'avatar_small' })
				),
				React.createElement(
					'div',
					null,
					this.props.name
				),
				React.createElement(
					'div',
					null,
					this.props.catchphrase
				),
				React.createElement(
					'div',
					null,
					this.props.games,
					React.createElement('br', null),
					'games'
				),
				React.createElement(
					'div',
					null,
					this.props.wins,
					React.createElement('br', null),
					'wins'
				)
			);
		}
	}]);

	return PlayerBar;
}(React.Component);

var Player = function Player(name, catchphrase, cueLength, uid) {
	_classCallCheck(this, Player);

	// defaults
	var names = ['John Doe', 'Jane Doe', 'John Smith', 'Jane Smith', 'Anne Chovie', 'Max Power', 'Burt Toast', 'Jay Walker', 'Gerry Atrick'];
	var catchphrases = ['Things are getting too spicy for the pepper!', 'Is that your final answer?', 'Resistance is futile.', 'Did I do that?', 'Find your own beach house, Crabby.'];
	var cueLengths = [42, 52, 56, 58, 60, 62];

	this.name = name || names.rand();
	this.catchphrase = catchphrase || catchphrases.rand();
	this.cueLength = cueLength || cueLengths.rand();
	this.games = 0;
	this.wins = 0;
	this.uid = uid || String(Date.now()); // quick pseudo-unique id
};

var Game = function Game(idA, idB, isWinnerA, nameA, nameB, catchphraseA, catchphraseB) {
	_classCallCheck(this, Game);

	this.idA = idA;
	this.idB = idB;
	this.isWinnerA = isWinnerA;
	this.nameA = nameA;
	this.nameB = nameB;
	this.catchphraseA = catchphraseA;
	this.catchphraseB = catchphraseB;
};

window.onload = function () {
	ReactDOM.render(React.createElement(App, null), document.getElementById('root'));
};
Array.prototype.rand = function () {
	return this[Math.floor(Math.random() * this.length)];
};